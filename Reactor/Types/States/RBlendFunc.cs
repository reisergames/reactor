﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reactor.Types.States
{
    public enum RBlendFunc
    {
        Add,
        Subtract,
        ReverseSubtract,
        Max,
        Min,
    }
}
