﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Reactor.Types;

namespace Reactor
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RViewport
    {
        public float X;
        public float Y;
        public float Width;
        public float Height;
        public float AspectRatio;
        public RViewport(int x, int y)
        {
            X = x;
            Y = y;
            Width = 800;
            Height = 600;
            AspectRatio = (float)Width / (float)Height;
        }

        public RViewport(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            AspectRatio = Width / Height;
        }

        internal void Bind()
        {
            AspectRatio = Width / Height;
            GL.Viewport((int)0, (int)0, (int)Width, (int)Height);
        }

    }
}
