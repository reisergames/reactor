﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reactor.Types
{
    public enum RLightType : int
    {
        DIRECTIONAL = 0,
        POINT = 1,
        SPOTLIGHT = 2
    }
}
