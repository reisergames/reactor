namespace Reactor.Math
{
    using System;

    public enum CurveContinuity
    {
        Smooth,
        Step
    }
}

