﻿
namespace Reactor.Math
{
    public enum ContainmentType
    {
        Disjoint,
        Contains,
        Intersects
    }
}

