﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reactor.Geometry
{
    public enum RVertexElementUsage
    {
        Position,
        Color,
        TextureCoordinate,
        Normal,
        Bitangent,
        Tangent,
        BlendIndices,
        BlendWeight,
        Depth,
        Fog,
        PointSize,
        Sample,
        TessellateFactor
    }
}
