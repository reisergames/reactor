﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reactor.Geometry
{
    public interface IVertexType
    {
        RVertexDeclaration Declaration
        {
            get;
        }
    }
}
