﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reactor.Geometry
{
    public enum RVertexElementFormat
    {
        Single,
        Vector2,
        Vector3,
        Vector4,
        Color,
        Byte4,
        Short2,
        Short4,
        NormalizedShort2,
        NormalizedShort4,
        HalfVector2,
        HalfVector4
    }
}
